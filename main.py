import math

from nltk.corpus import stopwords
import re


stops = set(stopwords.words('russian'))


# очистка новостной страницы от стоп слов и фильтрация от повторений
with open("news_page.txt", encoding="utf-8") as r, open("cleared_page.txt", mode="w", encoding='utf-8') as w:
    text = r.read()
    word_list = re.findall(r"[\w’]+", text)
    for i in range(len(word_list)):
        word_list[i] = word_list[i].lower()
    word_set = set(word_list)
    word_set = word_set - stops
    for word in word_set:
        w.write(word + " ")

# создание списка слов-ассоциаций с новостями
with open("news_related_words.txt", encoding="utf-8") as r:
    text = r.read()
    news_related_words = set(text.split(" "))

# создание списка слов-ассоциаций со спортом
with open("sport_related_words.txt", encoding="utf-8") as r:
    text = r.read()
    sport_related_words = set(text.split(" "))

# создание списка слов-ассоциаций с шоппингом
with open("shopping_related_words.txt", encoding="utf-8") as r:
    text = r.read()
    shopping_related_words = set(text.split(" "))

# создание списка слов-ассоциаций с наукой
with open("science_related_words.txt", encoding="utf-8") as r:
    text = r.read()
    science_related_words = set(text.split(" "))


def jaccard(word_set: set, category_related_words: set):
    intersection = word_set.intersection(category_related_words)
    union = word_set.union(category_related_words)
    return len(intersection) / len(union)


def cosine_similarity(word_list: list, category_related_words: set):
    union = list(set(word_list).union(category_related_words))
    matrix = [[0] * len(union) for _ in range(2)]

    union_dict = {union[i]: i for i in range(len(union))}

    # заполнение первого вектора
    for i in range(len(word_list)):
        matrix[0][union_dict[word_list[i]]] += 1

    # заполнение второго вектора
    for i in range(len(category_related_words)):
        matrix[1][union_dict[list(category_related_words)[i]]] = 1

    # Вывод векторов (визуализация)
    # for i in range(2):
    #     for j in range(len(union)):
    #         print(matrix[i][j], end=' ')
    #     print()

    vector_product = 0
    length_1 = 0
    length_2 = 0

    for i in range(len(union)):
        vector_product += matrix[0][i] * matrix[1][i]
        length_1 += matrix[0][i]**2
        length_2 += matrix[1][i]**2
    length_1 = math.sqrt(length_1)
    length_2 = math.sqrt(length_2)


    cos = vector_product / (length_1 * length_2)

    return cos



print("Jaccard indices:\n")
print(f"News:      {jaccard(word_set, news_related_words)}")
print(f"Sport:     {jaccard(word_set, sport_related_words)}")
print(f"Shopping:  {jaccard(word_set, shopping_related_words)}")
print(f"Science:   {jaccard(word_set, science_related_words)}")
print("\n\n")

print("Cosine similarity:\n")
print(f"News:      {cosine_similarity(word_list, news_related_words)}")
print(f"Sport:     {cosine_similarity(word_list, sport_related_words)}")
print(f"Shopping:  {cosine_similarity(word_list, shopping_related_words)}")
print(f"Science:   {cosine_similarity(word_list, science_related_words)}")
